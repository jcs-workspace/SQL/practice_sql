/*
  Service Platform Data Transfer

  Source Server         : $server_name$
  Source Server Type    : $server_type$
  Source Server Version : $server_version$
  Source Host           : $hostname$
  Source Schema         : $schema$

  Target Server Type    : $server_type$
  Target Server Version : $server_version$
  File Encoding         : $file_encoding$

  Date: 2024-05-02 01:13:55
 */

CREATE TABLE customer (
  first_name VARCHAR(30) NOT NULL,
  last_name VARCHAR(30) NOT NULL,
  email VARCHAR(60) NOT NULL,
  company VARCHAR(60) NOT NULL,
  street VARCHAR(50) NOT NULL,
  city VARCHAR(40) NOT NULL,
  state CHAR(2) NOT NULL,
  zip SMALLINT NOT NULL,
  phone VARCHAR(20) NOT NULL,
  birth_date DATE NULL,
  sex CHAR(1) NOT NULL,
  date_entered TIMESTAMP NOT NULL,
  id SERIAL PRIMARY KEY
);

INSERT INTO customer (
  first_name, last_name, email, company, street, city, state, zip,
  phone, birth_date, sex, date_entered, id)
VALUES ('Chirstopher', 'Jones', 'christopherjones@jb.com', 'BP', '347 Cedar St',
        'Lawrenceville', 'GA', '30044', '348-848-9292', '1928-09-11', 'M',
        current_timestamp);

CREATE TYPE sex_type as enum ('M', 'F');

ALTER TABLE customer
  ALTER COLUMN sex TYPE sex_type USING sex::sex_type;

CREATE TABLE product_type (
  name VARCHAR(30) NOT NULL,
  id SERIAL PRIMARY KEY
);

CREATE TABLE product (
  type_id INTEGER REFERENCES product_type(id),
  name VARCHAR(30) NOT NULL,
  supplier VARCHAR(30) NOT NULL,
  description TEXT NOT NULL,
  id SERIAL PRIMARY KEY
);

CREATE TABLE item (
  product_id INTEGER REFERENCES product(id),
  size INTEGER NOT NULL,
  color VARCHAR(30) NOT NULL,
  picture VARCHAR(256) NOT NULL,
  price NUMERIC(6, 2) NOT NULL,
  id SERIAL PRIMARY KEY
);

CREATE TABLE sales_item (
  item_id INTEGER REFERENCES item(id),
);

SELECT * FROM customer;
