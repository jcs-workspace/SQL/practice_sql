/*
  Service Platform Data Transfer

  Source Server         : $server_name$
  Source Server Type    : $server_type$
  Source Server Version : $server_version$
  Source Host           : $hostname$
  Source Schema         : $schema$

  Target Server Type    : $server_type$
  Target Server Version : $server_version$
  File Encoding         : $file_encoding$

  Date: 2024-05-03 23:32:06
 */

CREATE TABLE account (
  username VARCHAR(30) NOT NULL,
  password VARCHAR(30) NOT NULL,
  email VARCHAR(60) NOT NULL,
  create_date TIMESTAMP NOT NULL,
  sex CHAR(1) NOT NULL,
  admin CHAR(1) NOT NULL DEFAULT '0',
  id SERIAL PRIMARY KEY
);

INSERT INTO account (
  username, password, email, create_date, sex, admin)
VALUES ('johnthan', 'abcd1234', 'example@gg.com', current_timestamp, 'M', 'true');

SELECT *
  FROM customers
 WHERE shipped IS NULL;

SELECT *
  FROM customers
 ORDER BY first_name DESC;

CREATE TABLE order_items (
  order_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  quantity INTEGER NOT NULL,
  unit_price FLOAT NOT NULL
);

INSERT INTO order_items (order_id, product_id, quantity, unit_price)
VALUES (2, 1, 2, 9.10);

INSERT INTO order_items (order_id, product_id, quantity, unit_price)
VALUES (2, 4, 4, 1.66);

INSERT INTO order_items (order_id, product_id, quantity, unit_price)
VALUES (2, 6, 2, 2.94);

SELECT *, quantity * unit_price AS total_price
  FROM order_items
 WHERE order_id = 2
 ORDER BY total_price DESC;
